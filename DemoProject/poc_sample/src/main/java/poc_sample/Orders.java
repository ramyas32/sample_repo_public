package poc_sample;

import java.io.Serializable;

public class Orders implements Serializable{

	/**
	 * test
	 */
	private static final long serialVersionUID = 1L;
	String id;
	String customer;
	String email;
	Long contactNumber;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomer() {
		return customer;
	}
	public Orders() {
		super();
		// TODO Auto-generated constructor stub
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}
}
