package poc_sample;

import java.io.Serializable;

public class ProfileDetails implements Serializable{
	public ProfileDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String firstName;
	String lastName;
	Boolean active;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public ProfileDetails(String firstName, String lastName, Boolean active) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.active = active;
	}
}
