package poc_sample;

import java.io.Serializable;

public class ProfileKey implements Serializable{
	public ProfileKey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProfileKey(int id) {
		super();
		this.id = id;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
